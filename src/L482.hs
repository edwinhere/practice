module L482 where

import           Control.Monad.State
import           Data.Char
import           Data.List

licenseKeyFormatting :: String -> Int -> String
licenseKeyFormatting s k = let
  clean = reverse . filter (/= '-') $ fmap toUpper s
  r = length clean `mod` k
  in evalState (loop clean k r) [[]]

loop :: String -> Int -> Int -> State [String] String
loop [] _ _ = do
  modify reverse
  result <- get
  return . intercalate "-" $ fmap reverse result
loop s@(c:cs) k r = do
  result <- get
  let
    lResult = length $ last result
    lRemaining = length s
  if lResult == k && lRemaining /= r
    then do
      put $ result ++ [[c]]
      loop cs k r
    else if lRemaining == r
      then do
        put $ result ++ [s]
        loop [] k r
      else do
        put $ init result ++ [last result ++ [c]]
        loop cs k r

test :: IO ()
test = do
  print $ licenseKeyFormatting "2-4A0r7-4k" 4
  print $ licenseKeyFormatting "2-4A0r7-4k" 3
