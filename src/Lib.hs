module Lib where

import           Prelude (error, id, ($), (.))

data Maybe a = Just a | Nothing
data Either a b = Left a | Right b

-- show Functor instances
class Functor f where
  fmap :: (a -> b) -> f a -> f b

-- Exercise 1
-- Relative Difficulty: 1
instance Functor [] where
--fmap :: (a -> b) -> [a] -> [b]
  fmap _ [] = []
  fmap f (x:xs) = f x : fmap f xs

-- Exercise 2
-- Relative Difficulty: 1
instance Functor Maybe where
--fmap :: (a -> b) -> Maybe a -> Maybe b
  fmap _ Nothing = Nothing
  fmap f (Just a) = Just $ f a

-- Exercise 3
-- Relative Difficulty: 5
instance Functor ((->) t) where
--fmap :: (a -> b) -> (t -> a) -> (t -> b)
  fmap = (.)

newtype EitherLeft b a = EitherLeft (Either a b)
newtype EitherRight a b = EitherRight (Either a b)

-- Exercise 4
-- Relative Difficulty: 5
instance Functor (EitherLeft t) where
--fmap :: (a -> b) -> EitherLeft t a -> EitherLeft t b
  fmap f (EitherLeft (Left a)) = EitherLeft (Left (f a))
  fmap f (EitherLeft (Right t)) = EitherLeft (Right t)

-- Exercise 5
-- Relative Difficulty: 5
instance Functor (EitherRight t) where
--fmap :: (a -> b) -> EitherRight t a -> EitherRight t b
  fmap f (EitherRight (Left t)) = EitherRight (Left t)
  fmap f (EitherRight (Right a)) = EitherRight (Right (f a))

-- show Additional Monad functions
class Monad m where
  (=<<) :: (a -> m b) -> m a -> m b
  return :: a -> m a

-- Exercise 7
-- Relative Difficulty: 2
instance Monad [] where
--(=<<) :: (a -> [b]) -> [a] -> [b]
  (=<<) f = join . fmap f
--return :: a -> [a]
  return = (:[])

-- Exercise 8
-- Relative Difficulty: 2
instance Monad Maybe where
--(=<<) :: (a -> Maybe b) -> Maybe a -> Maybe b
  (=<<) _ Nothing = Nothing
  (=<<) f (Just a) = f a
--return :: a -> Maybe a
  return = Just

-- Exercise 9
-- Relative Difficulty: 6
instance Monad ((->) t) where
--(=<<) :: (a -> t -> b) -> (t -> a) -> (t -> b)
  (=<<) f g t = f (g t) t
--return :: a -> (t -> a)
  return a t = a

-- Exercise 10
-- Relative Difficulty: 6
instance Monad (EitherLeft t) where
--(=<<) :: (a -> EitherLeft t b) -> EitherLeft t a -> EitherLeft t b
  (=<<) f (EitherLeft (Left a)) = f a
  (=<<) f (EitherLeft (Right t)) = EitherLeft (Right t)
--return :: a -> EitherLeft t a
  return = EitherLeft . Left

-- Exercise 11
-- Relative Difficulty: 6
instance Monad (EitherRight t) where
--(=<<) :: (a -> EitherRight t b) -> EitherRight t a -> EitherRight t b
  (=<<) f (EitherRight (Left t)) = EitherRight (Left t)
  (=<<) f (EitherRight (Right a)) = f a
--return :: a -> EitherRight t a
  return = EitherRight . Right


-- Exercise 6
-- Relative Difficulty: 3
-- (use (=<<) and/or return)
fmap' :: (Monad m) => (a -> b) -> m a -> m b
fmap' f = (=<<) (return . f)

-- Exercise 12
-- Relative Difficulty: 3
join :: (Monad m) => m (m a) -> m a
join = (=<<) id

-- Exercise 13
-- Relative Difficulty: 6
(<**>) :: (Monad m) => m a -> m (a -> b) -> m b
(<**>) ma mab = (=<<) (\a -> (=<<) (\ab -> return (ab a)) mab) ma

-- Exercise 14
-- Relative Difficulty: 6
forM :: (Monad m) => [a] -> (a -> m b) -> m [b]
forM [] _ = return []
forM (a:as) f = liftM2 (:) (f a) (forM as f)

-- Exercise 15
-- Relative Difficulty: 6
-- (bonus: use forM)
sequence :: (Monad m) => [m a] -> m [a]
sequence mas = forM mas id
--sequence [] = return []
--sequence (ma:mas) = liftM2 (:) ma (sequence mas)

-- Exercise 16
-- Relative Difficulty: 6
-- (bonus: use (<**>) + fmap')
liftM2 :: (Monad m) => (a -> b -> c) -> m a -> m b -> m c
--liftM2 f ma mb = (=<<) (\a -> (=<<) (\b -> return (f a b)) mb) ma
liftM2 f ma mb = mb <**> fmap' f ma

-- Exercise 17
-- Relative Difficulty: 6
-- (bonus: use (<**>) + liftM2)
liftM3 :: (Monad m) => (a -> b -> c -> d) -> m a -> m b -> m c -> m d
liftM3 f ma mb mc = mc <**> liftM2 f ma mb

-- Exercise 18
-- Relative Difficulty: 6
-- (bonus: use (<**>) + liftM3)
liftM4 :: (Monad m) => (a -> b -> c -> d -> e) -> m a -> m b -> m c -> m d -> m e
liftM4 f ma mb mc md = md <**> liftM3 f ma mb mc

newtype State s a = State {
  state :: s -> (s, a)
}

-- Exercise 19
-- Relative Difficulty: 9
instance Functor (State s) where
--fmap :: (a -> b) -> State s a -> State s b
  fmap f (State ssa) = State $ \s -> let
    (s', a) = ssa s
    in (s', f a)

-- Exercise 20
-- Relative Difficulty: 10
instance Monad (State s) where
--(=<<) :: (a -> State s b) -> State s a -> State s b
  (=<<) f (State ssa) = State $ \s -> let
    (s', a) = ssa s
    (State ssb) = f a
    in ssb s'
--return :: a -> State s a
  return a = State $ \s -> (s, a)
