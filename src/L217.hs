module L217 where

import           Data.List

containsDuplicate :: [Int] -> Bool
containsDuplicate is = length is /= length (nub is)

test :: IO ()
test = do
  print $ containsDuplicate [1,2,3,4,5,6,7,8,9,0]
  print $ containsDuplicate []
  print $ containsDuplicate [3,1,4,1]
