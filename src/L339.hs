module L339 (test) where

data NestedInteger = I Integer | N [NestedInteger]

depthSum :: [NestedInteger] -> Integer
depthSum = depthSum' 1

depthSum' :: Integer -> [NestedInteger] -> Integer
depthSum' depth [] = 0
depthSum' depth (I i:ns) = depth * i + depthSum' depth ns
depthSum' depth (N n:ns) = depthSum' (depth + 1) n + depthSum' depth ns

example1 :: Bool
example1 = depthSum [N [I 1,I 1],I 2, N [I 1, I 1]] == 10

example2 ::Bool
example2 = depthSum [I 1,N [I 4,N [I 6]]] == 27

test :: Bool
test = example1 && example2
