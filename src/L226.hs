module L226 where

import           Debug.Trace

isPalindrome :: String -> Bool
isPalindrome as = reverse as == as

permutations :: String -> [String]
permutations [] = []
permutations [x] = [[x]]
permutations (x:xs) = let
  tails :: [String]
  tails = permutations xs
  appends :: [String]
  appends = fmap (++ [x]) tails
  prepends :: [String]
  prepends = fmap ([x] ++) tails
  in appends ++ prepends

palindromable :: String -> Bool
palindromable as = or $ fmap isPalindrome (permutations as)

example1 :: Bool
example1 = not $ palindromable "code"

example2 :: Bool
example2 = palindromable "aab"

example3 :: Bool
example3 = palindromable "carerac"

test :: IO ()
test = print $ example1 && example2 && example3
