module L283 where

moveZeroes :: [Int] -> [Int]
moveZeroes is = let
    nonZeroes = filter (/= 0) is
    zeroes = filter (== 0) is
    in nonZeroes ++ zeroes

test :: IO ()
test = print $ moveZeroes [0, 1, 0, 3, 12]
