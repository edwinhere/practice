module L388 where

import           Control.Monad.State
import           Data.List

type Path = String

data LoopState = LoopState {
        stack  :: [Int],
        maxLen :: Int
    }

lengthLongestPath :: String -> Int
lengthLongestPath input = let
    paths = lines input
    _stack = replicate (length paths + 1) 0
    _maxLen = 0
    initialState = LoopState _stack _maxLen
    in maxLen $ execState (loop paths) initialState

loop :: [Path] -> State LoopState ()
loop [] = return ()
loop (s:ss) = do
    localVariables <- get
    let
        _stack :: [Int]
        _stack = stack localVariables
        _maxLen :: Int
        _maxLen = maxLen localVariables
        indices :: [Int]
        indices = elemIndices '\t' s
        lev :: Int
        lev = if null indices
          then 0
          else last indices
        new :: Int
        new = _stack !! lev + length s - lev + 1
        newStack :: [Int]
        newStack = update _stack (lev + 1) new
        curLen :: Int
        curLen = _stack !! (lev + 1)
        newMaxLen = if '.' `elem` s
            then max _maxLen (curLen - 1)
            else _maxLen
        newLocalVariables = localVariables { stack = newStack, maxLen = newMaxLen }
    put newLocalVariables
    loop ss



update :: [Int] -> Int -> Int -> [Int]
update input index new = let
     before = take index input
     after = drop (index + 1) input
     in before ++ [new] ++ after

test :: IO ()
test = print $ lengthLongestPath "dir/subdir2/subsubdir2/file2.ext"
