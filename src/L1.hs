module L1 where

import           Data.List

twoSum :: [Int] -> Int -> (Int, Int)
twoSum nums target = let
    l = length nums - 1
    cartesian = [(i, j) | i <- [0..l], j <- [0..l], i < j, nums !! i + nums !! j == target]
    in head cartesian

test :: IO ()
test = print $ twoSum [2, 7, 11, 15] 9 == (0,1)
