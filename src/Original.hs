module Original where

import           Prelude (error, ($))

data Maybe a = Just a | Nothing
data Either a b = Left a | Right b

-- show Functor instances
class Functor f where
  fmap :: (a -> b) -> f a -> f b

-- Exercise 1
-- Relative Difficulty: 1
instance Functor [] where
  fmap = error "todo"

-- Exercise 2
-- Relative Difficulty: 1
instance Functor Maybe where
  fmap = error "todo"

-- Exercise 3
-- Relative Difficulty: 5
instance Functor ((->) t) where
  fmap = error "todo"

newtype EitherLeft b a = EitherLeft (Either a b)
newtype EitherRight a b = EitherRight (Either a b)

-- Exercise 4
-- Relative Difficulty: 5
instance Functor (EitherLeft t) where
  fmap = error "todo"

-- Exercise 5
-- Relative Difficulty: 5
instance Functor (EitherRight t) where
  fmap = error "todo"

-- show Additional Monad functions
class Monad m where
  (=<<) :: (a -> m b) -> m a -> m b
  return :: a -> m a

-- Exercise 7
-- Relative Difficulty: 2
instance Monad [] where
  (=<<) = error "todo"
  return = error "todo"

-- Exercise 8
-- Relative Difficulty: 2
instance Monad Maybe where
  (=<<) = error "todo"
  return = error "todo"

-- Exercise 9
-- Relative Difficulty: 6
instance Monad ((->) t) where
  (=<<) = error "todo"
  return = error "todo"

-- Exercise 10
-- Relative Difficulty: 6
instance Monad (EitherLeft t) where
  (=<<) = error "todo"
  return = error "todo"

-- Exercise 11
-- Relative Difficulty: 6
instance Monad (EitherRight t) where
  (=<<) = error "todo"
  return = error "todo"


-- Exercise 6
-- Relative Difficulty: 3
-- (use (=<<) and/or return)
fmap' :: (Monad m) => (a -> b) -> m a -> m b
fmap' = error "todo"

-- Exercise 12
-- Relative Difficulty: 3
join :: (Monad m) => m (m a) -> m a
join = error "todo"

-- Exercise 13
-- Relative Difficulty: 6
(<**>) :: (Monad m) => m a -> m (a -> b) -> m b
(<**>) = error "todo"

-- Exercise 14
-- Relative Difficulty: 6
forM :: (Monad m) => [a] -> (a -> m b) -> m [b]
forM = error "todo"

-- Exercise 15
-- Relative Difficulty: 6
-- (bonus: use forM)
sequence :: (Monad m) => [m a] -> m [a]
sequence = error "todo"

-- Exercise 16
-- Relative Difficulty: 6
-- (bonus: use (<**>) + fmap')
liftM2 :: (Monad m) => (a -> b -> c) -> m a -> m b -> m c
liftM2 = error "todo"

-- Exercise 17
-- Relative Difficulty: 6
-- (bonus: use (<**>) + (=<<)2)
liftM3 :: (Monad m) => (a -> b -> c -> d) -> m a -> m b -> m c -> m d
liftM3 = error "todo"

-- Exercise 18
-- Relative Difficulty: 6
-- (bonus: use (<**>) + (=<<)3)
liftM4 :: (Monad m) => (a -> b -> c -> d -> e) -> m a -> m b -> m c -> m d -> m e
liftM4 = error "todo"

newtype State s a = State {
  state :: s -> (s, a)
}

-- Exercise 19
-- Relative Difficulty: 9
instance Functor (State s) where
  fmap = error "todo"

-- Exercise 20
-- Relative Difficulty: 10
instance Monad (State s) where
  (=<<) = error "todo"
  return = error "todo"
