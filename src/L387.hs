module L387 where

import           Data.List

firstUniqChar :: String -> Int
firstUniqChar cs = let
    withIndices = zip cs [0..]
    f :: [(Char, Int)] -> Int
    f [] = -1
    f [x] = snd x
    f (x:xs) = if fst x `elem` fmap fst xs
        then f (filter (\y -> fst y /= fst x) xs)
        else snd x
    in
        f withIndices

test :: IO ()
test = do
    print $ firstUniqChar "leetcode" == 0
    print $ firstUniqChar "loveleetcode" == 2
