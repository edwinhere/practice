module L445 where

import           Data.List

addTwoNumbers :: [Int] -> [Int] -> [Int]
addTwoNumbers a b = toList $ toInteger a + toInteger b
  where
    powers :: Int -> [Int]
    powers x = [ 10^i | i <- [0..(x - 1)]]
    toInteger :: [Int] -> Int
    toInteger is = sum . zipWith (*) is $ powers (length is)
    toList :: Int -> [Int]
    toList i
        | i < 10 = [i]
        | otherwise = toList (i `div` 10) ++ [i `mod` 10]
