module L136 where

import qualified Data.Map   as M
import           Data.Maybe

singleNumber :: [Integer] -> Integer
singleNumber is = let
    h = histogram M.empty is
    in head . M.keys $ M.filter (== 1) h

histogram :: M.Map Integer Integer -> [Integer] -> M.Map Integer Integer
histogram m [] = m
histogram m (i:is) = histogram (increment m i) is

increment :: M.Map Integer Integer -> Integer -> M.Map Integer Integer
increment m i = M.insert i (safeLookup m i + 1) m
    where
        safeLookup :: M.Map Integer Integer -> Integer -> Integer
        safeLookup m' i' = fromMaybe 0 $ M.lookup i' m'

test :: IO ()
test = print $ singleNumber [1,2,1] == 2
