module L359 where

import           Control.Monad.State
import qualified Data.Map            as M
import           Data.Maybe
import           Debug.Trace


type LogState = M.Map String Integer

shouldPrintMessage :: Integer -> String -> State LogState Bool
shouldPrintMessage t s = do
  logState <- get
  let
    lastPrintTime = s `M.lookup` logState
    shouldPrint = isNothing lastPrintTime || (t - fromJust lastPrintTime >= 10)
    newState = if shouldPrint
      then M.insert s t logState
      else logState
  put newState
  return shouldPrint

example :: State LogState [Bool]
example = do
  a <- shouldPrintMessage 1 "foo"
  b <- shouldPrintMessage 2 "bar"
  c <- shouldPrintMessage 3 "foo"
  d <- shouldPrintMessage 8 "bar"
  e <- shouldPrintMessage 10 "foo"
  f <- shouldPrintMessage 11 "foo"
  return [a,b,c,d,e,f]

test :: IO ()
test = do
  let
    (result, _) = runState example M.empty
  print result
