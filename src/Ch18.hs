{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Ch18 where

import           Control.Monad
import           Control.Monad.Reader
import           Control.Monad.State
import           Control.Monad.Writer
import           System.Directory
import           System.FilePath
import           System.IO

countEntriesM :: FilePath -> IO [(FilePath, Int)]
countEntriesM path = do
  contents <- listDirectory path
  rest <- forM contents $ \p -> do
    let newPath = path </> p
    isDir <- doesDirectoryExist newPath
    if isDir
      then countEntriesM newPath
      else return []
  return $ (path, length contents) : concat rest

countEntriesT :: FilePath -> WriterT [(FilePath, Int)] IO ()
countEntriesT path = do
  contents <- liftIO $ listDirectory path
  tell [(path, length contents)]
  forM_ contents $ \name -> do
    let newPath = path </> name
    isDir <- liftIO $ doesDirectoryExist newPath
    when isDir $ countEntriesT newPath

testCountEntriesT :: IO ()
testCountEntriesT = execWriterT (countEntriesT "..") >>= print

newtype AppConfig = AppConfig {
    cfgMaxDepth :: Int
  }

newtype AppState = AppState {
    stDeepestReached :: Int
  }

--type App = ReaderT AppConfig (StateT AppState IO)
newtype App a = App {
    runA :: ReaderT AppConfig (StateT AppState IO) a
  } deriving (MonadReader AppConfig, MonadState AppState, MonadIO, Monad, Applicative, Functor)

runApp :: App a -> Int -> IO (a, AppState)
runApp k maxDepth =
  let config = AppConfig maxDepth
      state = AppState 0
  in runStateT (runReaderT (runA k) config) state

deepDive :: Int -> FilePath -> App ()
deepDive curDepth path = do
  contents <- liftIO $ listDirectory path
  cfg <- ask
  forM_ contents $ \name -> do
    let newPath = path </> name
    isDir <- liftIO $ doesDirectoryExist newPath
    when (isDir && curDepth < cfgMaxDepth cfg) $ do
        let newDepth = curDepth + 1
        st <- get
        when (stDeepestReached st < newDepth) $
          put st { stDeepestReached = newDepth }
        deepDive newDepth newPath
