module L3 where

import           Data.List
import           Data.List.Split
import           Debug.Trace

lengthOfLongestSubstring :: String -> Int
lengthOfLongestSubstring s = let
  substrings :: [String]
  substrings = filter (`isInfixOf` s) (subsequences s)
  hasNoDuplicates :: String -> Bool
  hasNoDuplicates s = length s == length (nub s)
  unequals :: [String]
  unequals = filter hasNoDuplicates substrings
  byLength :: String -> String -> Ordering
  byLength a b = length a `compare` length b
  longest :: String
  longest = maximumBy byLength unequals
  in length longest
