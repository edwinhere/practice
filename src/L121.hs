module L121 where

maxProfit :: [Int] -> Int
maxProfit prices = let
  withIndices = zip prices [0..]
  selfJoin = [ p2 - p1 | (p1, t1) <- withIndices, (p2, t2) <- withIndices, t1 < t2, p1 < p2]
  in if null selfJoin
    then 0
    else maximum selfJoin

test :: IO ()
test = do
  print $ maxProfit [7, 1, 5, 3, 6, 4]
  print $ maxProfit [7, 6, 4, 3, 1]
