module L202 where

import           Control.Monad.State
import qualified Data.Map            as M
import           Debug.Trace

toDigits :: Integer -> [Integer]
toDigits = fmap toDigit . show
  where
    toDigit :: Char -> Integer
    toDigit c = read [c] :: Integer

sumSquare :: [Integer] -> Integer
sumSquare is = sum $ fmap square is
  where
    square :: Integer -> Integer
    square = (^ 2)

isHappy' :: Integer -> State [Integer] Bool
isHappy' i
  | sumSquare (toDigits i) == 1 = return True
  | otherwise = do
    history <- get
    if i `elem` history
      then return False
      else do
        put (i:history)
        isHappy' (sumSquare (toDigits i))

isHappy :: Integer -> Bool
isHappy i = evalState (isHappy' i) []
